var express = require('express');
const fetch = require('node-fetch');
const mongoose = require('mongoose');
const moment = require('moment');
var cors = require('cors')

var lastCnx = require('../models/lastcnx');
var Hit = require('../models/hits');

var router = express.Router();

//we need to check if one hour passed since last use
// if more than 1 hour retrieve data from url http://hn.algolia.com/api/v1/search_by_date?query=nodejs
// store the new data in the mongodb database
// else retrieve the last stored data from mongodb database 
// anyway output the json data from hn feed.

const mongoURI = 'mongodb://root:therootpwd@mongo:27017/hnfeed'; //in docker
//const mongoURI = 'mongodb://root:therootpwd@localhost:27017/hnfeed';
var mongo = null;


async function createCnxDB() {
  await mongoose.connect(mongoURI, {
    useNewUrlParser: true
  });
  return mongoose.connection;
}

async function moreThanOneHour(res) {
  mongo.db.listCollections({ name: 'lastcnx' }).toArray(function (err, collectionNames) {
    if (err) {
      console.log(err);
    }
    console.log('collection list: ', collectionNames);
    if (collectionNames.length === 0) {
      console.log('creando colleccion lastcnx');
      let nowdate = moment(new Date()).utc();
      let lastRefresh = new lastCnx({ datecnx: nowdate });
      lastRefresh.save((err) => {
        console.log("document has been saved in lastcnx collection !");
        if (err) {
          console.error(err);
        }
      })
    } else {
      console.log('collection lastcnx exist we need to read the document in it')
      // leer de la base de datos.
      lastCnx.findOne({}, {}, { sort: { 'created_at': -1 } }, async function (err, date) {
        let testdate_utc = moment(date.datecnx).utc().add(1, 'hours');
        let nowdate_utc = moment(new Date()).utc();
        moment(nowdate_utc).isSameOrAfter(testdate_utc);
        console.log('date from data base: ', date.datecnx);
        console.log('date from db +1 hour: ', testdate_utc.format());
        console.log('date now', nowdate_utc.format());
        let testresult = moment(nowdate_utc).isSameOrAfter(testdate_utc);
        console.log('result of test date:', testresult);
        if (testresult) {
          //
          let doc = await lastCnx.findOneAndUpdate({ datecnx: date.datecnx }, { datecnx: nowdate_utc }, {
            returnOriginal: false,
            useFindAndModify: false
          });
          console.log("document has been updated and saved in lastcnx collection !");
         await readWebHitsStoreDb(res);
        } else {
         await processHits(res);
        }

      });
    }
  });
}

async function storeHitsDb(webhits) {
  let convertedHits = [];
  for (let h of webhits) {
    let newhit = new Hit({
      title: h.title,
      url: h.url,
      author: h.author,
      points: h.points,
      story_text: h.story_text,
      comment_text: h.comment_text,
      num_comments: h.num_comments,
      story_id: h.story_id,
      story_title: h.story_title,
      story_url: h.story_url,
      created_at: h.created_at
    });
    convertedHits.push(newhit);
  }
  await Hit.collection.insertMany(convertedHits);
}

async function readWebHitsStoreDb(res) {
  console.log('readWebHitsStoreDb');
  let webHits = await getDataFromURL();
  await storeHitsDb(webHits.hits);
  await processHits(res);
}

async function processHits(res) {
  console.log('processHits');
    mongo.db.listCollections({ name: 'hits' }).toArray(async function (err, collectionNames) {
      if (err) {
        console.log(err);
        return false;
      }
      if (collectionNames.length === 0) {
        console.log('collection hits does not exists!')
        await readWebHitsStoreDb();
        let foundHits = await Hit.find().sort({ $natural: -1 }).limit(20).exec((err, hits) => {
          console.log('send hits to be rendered on UI');
          mongo.close();
          res.json({hits: hits});
        });
      } else {
        console.log('collection hits exists!')
        Hit.find().sort({ $natural: -1 }).limit(20).lean().exec((err, hits) => {
          console.log('send hits to be rendered on UI');
          mongo.close();
          res.json({hits: hits}); 
        });
      }
    });
}

async function getDataFromURL() {
  let url = 'http://hn.algolia.com/api/v1/search_by_date?query=nodejs';
  const response = await fetch(url);
  const json = response.json();
  return json;
}

async function asyncEvalHits(res) {
  mongo = await createCnxDB();
 return  await moreThanOneHour(res);
}

router.get('/', cors(), async function (req, res, next) {
   await asyncEvalHits(res)
});

module.exports = router;