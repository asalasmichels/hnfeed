var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var hitschema = new Schema({
    title: String,
    url: String,
    author: String,
    points: String,
    story_text: String,
    comment_text: String,
    num_comments: Number,
    story_id: Number,
    story_title: String,
    story_url: String,
    created_at: Date
  });

 module.exports = mongoose.models.hits || mongoose.model('hits', hitschema, 'hits');