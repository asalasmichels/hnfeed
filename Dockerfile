#server restfull api
FROM mhart/alpine-node:latest as api
RUN mkdir /src
RUN npm install nodemon -g
WORKDIR /src
EXPOSE 3000


#front-end angular
FROM mhart/alpine-node:latest as angular
RUN mkdir /src
RUN npm install @angular/cli -g
WORKDIR /src
EXPOSE 4200

#front-end react
FROM  mhart/alpine-node:latest as react
RUN mkdir /src
WORKDIR /src
RUN npm install react-scripts@3.4.1 -g
EXPOSE 3000

