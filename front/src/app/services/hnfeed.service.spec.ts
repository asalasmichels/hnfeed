import { TestBed } from '@angular/core/testing';

import { HnfeedService } from './hnfeed.service';

describe('HnfeedService', () => {
  let service: HnfeedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HnfeedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
