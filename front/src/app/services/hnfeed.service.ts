import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HnfeedService {

  constructor( protected http: HttpClient) { }
  getHnFeed() {
    return this.http.get('http://localhost:7001/get-listhn');
  }

}
