import { Component, OnInit } from '@angular/core';
import { HnfeedService } from './services/hnfeed.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 //* */ 
  title='hnfeed';
  hits: Object [];
  constructor(
    protected hnfeedserv: HnfeedService
  ){}

  ngOnInit(){
    this.hnfeedserv.getHnFeed().subscribe(
      (data) => {
        this.hits = data['hits'];
        console.log('got hits:', this.hits);
      },
      (err) =>{
        console.log(err);
      }
    );

  }
  goToLink(url: string){
    if(url){
      window.open(url, "_blank");
    }

  }
}
