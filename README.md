HN FEED project by Alphe Salas


I) Project architecture

I.I) Folder organisation
    api/ -> contains node/express.js service RESTful API
    persitence -> contains mongoDB files
    front/ -> contains ui in angular:latest
    front-react -> contains ui in react:latest

I.II) Docker organisation
    Dockerfile -> build neserary docker  images.
    docker-compose.yml -> describes containers
    init-mongo.js -> inicialize mongo db with default

I.III) API organisation
    localhost:7001/get-listhn will display webapp api json version.

II) Installation
II.I) Related software
  git
  docker 19.03.6
  docker-compose 1.26.0
II.II) Web app Install
$ docker-compose build


II) HOW to run
    The whole web-app will run with a single commande
$ docker-compose up  

    The web-app are stopped with
$ docker-compose down

III) Accessing web site
    The web site angular version is access through webbrowser using http://localhost:8082
    The web site react version is access through webbrowser using http://localhost:8080




