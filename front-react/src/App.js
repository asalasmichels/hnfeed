import React, { Component } from 'react';
import Header from './components/header'
import News from './components/news'

import './App.css';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = { news: [] }
  }


  componentDidMount() {
    fetch('http://localhost:7001/get-listhn', { method: 'GET' })
      .then(res => { console.log("app.js res:", res); return res.json(); })
      .then((data) => {
        console.log("app.js data:", data);
        this.setState({ news: data.hits });
      })
      .catch(console.log)
  }
  render() {
    return (
      <div>
        <Header />
        <News news={this.state.news} />
      </div>
    )
  }
}

export default App;
