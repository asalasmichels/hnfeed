import React from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { SvgIcon } from '@material-ui/core';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';

const goToLink = (url) => {
    window.open(url, '_blank')
}

const News = ({ news }) => {
    console.log(news);
    return (
        <div>
            { news.map((hit) => (
                <Table>
                    <TableBody>
                        <TableRow hover onClick={goToLink(hit.story_url)}
                        key={hit._id}>
                            <TableCell>
                                {hit.story_title ? hit.story_title  : ''}
                            </TableCell>
                            <TableCell>
                                {hit.title ? hit.title : '' }
                            </TableCell>
                            <TableCell>
                                {hit.author}
                            </TableCell>
                            <TableCell>
                                {hit.created_at}
                            </TableCell>
                            <TableCell >
                                <DeleteOutlinedIcon></DeleteOutlinedIcon>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            )

            )}

        </div>
    );
}
export default News